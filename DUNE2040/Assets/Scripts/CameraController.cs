﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject arm_camera;
    public float turnSpeed = 50f;
    public float moveSpeed = 10f;
    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            arm_camera.transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.Q)){
            arm_camera.transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.W)){
            arm_camera.transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.A)){
            arm_camera.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.S)){
            arm_camera.transform.Translate(-Vector3.forward * moveSpeed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.D)){
            arm_camera.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
    }
}
